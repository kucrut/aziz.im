# aziz.im

The source of https://dz.aziz.im/

## Why open source your site?

Because I love free & open source software. This site doesn't contain any secret (apart from the user crendetials which will never be shared 😜). I've learned a lot from FOSS (even [make my living](https://humanmade.com/who-we-are/dzikri-aziz/) off of it) over the years so it's only fair for me to give back.

## Prerequisite

Make sure you have [Docker](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/) installed on your system and that the docker service is running.

## Setup

### Proxy

- Clone [proxy](https://gitlab.com/wp-id/docker/traefik-proxy) and enter the directory:
  ```sh
  git clone https://gitlab.com/wp-id/docker/traefik-proxy
  cd proxy
  ```
- Start the proxy:
  ```sh
  docker-compose up -d
  ```

### Site

- Somewhere (_not_ inside the proxy dir you just cloned), clone this repo _recursively_:
  ```sh
  git clone --recursive https://gitlab.com/kucrut/aziz.im.git
  cd aziz.im
  ```
- Copy `.env.example` to `.env` and update the value of `DOMAIN` to some local domain, for example: `wp.local`.
- Add these lines to your system's `/etc/hosts`:
  ```
  127.0.0.1 wp.local
  ::1 wp.local
  ```
- Install composer packages:
  ```sh
  ./wpc/bin/composer install --ignore-platform-reqs
  ```
- Start it up:
  ```
  ./wpc/bin/up -e local
  ```
- Visit http://wp.local/wp-admin and setup WordPress.
- Set permalink structure in Settings > Permalinks to `/blog/%postname%/`.
- To stop the containers:
  ```sh
  ./wpc/bin/down
  ```
