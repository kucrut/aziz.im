#!/bin/sh

set -e

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

PROJECT_DIR="$( dirname "$( cd "$( dirname "$( type -p "$0" )" )" && pwd )" )"
BACKEND_DIR="${PROJECT_DIR}/services/backend"

echo "Installing plugins..."
${PROJECT_DIR}/bin/composer "install --no-dev --ignore-platform-reqs"

echo "Building image..."
docker pull $CI_REGISTRY_IMAGE || true
# Build latest
docker build -t $CI_REGISTRY_IMAGE --cache-from $CI_REGISTRY_IMAGE $BACKEND_DIR
docker push $CI_REGISTRY_IMAGE
# Tag image to match repo's latest tag
docker tag $CI_REGISTRY_IMAGE "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"

echo "Done!"
