<?php
/**
 * Plugin Name: aziz.im: Media
 * Description: Misc. stuff for media.
 * Author: Dzikri Aziz
 * Author URI: https://aziz.im
 */

// phpcs:disable PEAR.Functions.FunctionCallSignature.CloseBracketLine
// phpcs:disable PEAR.Functions.FunctionCallSignature.ContentAfterOpenBracket
// phpcs:disable PEAR.Functions.FunctionCallSignature.MultipleArguments

/**
 * Set attachment's default "Link To" value to 'file'
 */
add_filter( 'pre_option_image_default_link_type', function () {
	return 'file';
});

/**
 * Set maximum srcset image width
 *
 * @return int
 */
add_filter( 'max_srcset_image_width', function () {
	return 2880;
});
