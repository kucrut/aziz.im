<?php
/**
 * Plugin Name: aziz.im: Misc.
 * Description: Misc. Filters & Such.
 * Author: Dzikri Aziz
 * Author URI: https://aziz.im
 */

// phpcs:disable PEAR.Functions.FunctionCallSignature.CloseBracketLine
// phpcs:disable PEAR.Functions.FunctionCallSignature.ContentAfterOpenBracket
// phpcs:disable PEAR.Functions.FunctionCallSignature.MultipleArguments

// Disable script & style from wp-rest-filter.
add_action( 'wp_enqueue_scripts', function () {
	wp_dequeue_script( 'wp-rest-filter' );
	wp_dequeue_style( 'wp-rest-filter' );
}, 999 );

// Disable emoji "helpers".
add_action( 'init', function() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
} );
