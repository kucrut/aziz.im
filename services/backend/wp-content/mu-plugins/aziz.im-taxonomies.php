<?php
/**
 * Plugin Name: aziz.im: Taxonomies
 * Description: Custom taxonomies.
 * Author: Dzikri Aziz
 * Author URI: https://aziz.im
 */

// phpcs:disable PEAR.Functions.FunctionCallSignature.CloseBracketLine
// phpcs:disable PEAR.Functions.FunctionCallSignature.ContentAfterOpenBracket
// phpcs:disable PEAR.Functions.FunctionCallSignature.MultipleArguments

add_action( 'init', function () {
	$labels = [
		'name'              => _x( 'Places', 'taxonomy general name', ' aziz.im' ),
		'singular_name'     => _x( 'Place', 'taxonomy singular name', ' aziz.im' ),
		'search_items'      => __( 'Search Places', ' aziz.im' ),
		'all_items'         => __( 'All Places', ' aziz.im' ),
		'parent_item'       => __( 'Parent Place', ' aziz.im' ),
		'parent_item_colon' => __( 'Parent Place:', ' aziz.im' ),
		'edit_item'         => __( 'Edit Place', ' aziz.im' ),
		'update_item'       => __( 'Update Place', ' aziz.im' ),
		'add_new_item'      => __( 'Add New Place', ' aziz.im' ),
		'new_item_name'     => __( 'New Place Name', ' aziz.im' ),
		'not_found'         => __( 'No places found.', ' aziz.im' ),
	];

	$args = [
		'public'                => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'rewrite'               => [
			'slug' => 'place',
		],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'places',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	];

	register_taxonomy( 'place', [ 'post' ], $args );
} );

add_filter( 'bridge_post_taxonomies_map', function ( $map ) {
	$map['places'] = [
		'taxonomy' => 'place',
	];

	return $map;
} );
