<?php
/**
 * Extra configs for dev environment
 */

global $redis_server;

$redis_server = [
	'host' => 'redis',
	'port' => 6379,
];

define( 'WP_CACHE_KEY_SALT', $_ENV['WP_CACHE_KEY_SALT'] ?? 'secret' );
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'SCRIPT_DEBUG', true );
