<?php
/**
 * Extra configs for build/prod environment
 */

global $redis_server;

$redis_server = [
	'host' => 'redis',
	'port' => 6379,
];

define( 'WP_CACHE_KEY_SALT', $_ENV['WP_CACHE_KEY_SALT'] ?? 'secret' );
